﻿Module Module1
    Sub Main()
        Dim AppSettings As New Dictionary(Of String, String)
        Dim openWebUIResponse As Integer

        ' Define default settings
        AppSettings.Add("DestinationFolder", "\\Mars\Downloads\TorrentFiles")
        AppSettings.Add("WebUIAddress", "http://internal.jnf.me/deluge")

        ' Attempt to open settings file and use it override default settings
        Dim SettingsFile As String = System.AppDomain.CurrentDomain.BaseDirectory() & "TorrentApp.ini"

        If My.Computer.FileSystem.FileExists(SettingsFile) Then
            Dim objReader As New System.IO.StreamReader(SettingsFile)
            Dim SettingLine As String
            Dim SettingItem As Array
            Do While objReader.Peek() <> -1
                SettingLine = objReader.ReadLine
                SettingItem = Split(SettingLine, "=")
                AppSettings(SettingItem(0)) = SettingItem(1)
            Loop
        End If

        ' Sanity checks: has a torrent file been supplied, does it exist, and does the destination folder exist?
        Dim cArgs() As String = Environment.GetCommandLineArgs()
        If cArgs.Count() < 2 Then GoTo EpicFail
        If My.Computer.FileSystem.FileExists(cArgs(1)) = False Then GoTo EpicFail
        If My.Computer.FileSystem.DirectoryExists(AppSettings("DestinationFolder")) = False Then GoTo EpicFail

        ' Move the file
        Dim tFile As System.IO.FileInfo = My.Computer.FileSystem.GetFileInfo(cArgs(1))
        My.Computer.FileSystem.MoveFile(cArgs(1), AppSettings("DestinationFolder") & "\" & tFile.Name)

        openWebUIResponse = MsgBox("Your download has been passed to the server for action." & vbCrLf & vbCrLf & "Would you like to view its progress?", MsgBoxStyle.Information + MsgBoxStyle.YesNo, "Downloading...")
        GoTo WeAreDone

EpicFail:
        openWebUIResponse = MsgBox("An unexpected error has occurred, and your download cannot be processed." & vbCrLf & vbCrLf & "Would you like to open the web interface to try and download it yourself?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, "Error")

WeAreDone:
        ' Display the WebUI if requested
        If openWebUIResponse = 6 Then Process.Start(AppSettings("WebUIAddress"))
    End Sub
End Module
