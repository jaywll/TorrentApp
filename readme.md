# TorrentApp #
TorrentApp is a simple VisualBasic 2010 application for handling torrent file downloads in Windows and passing them to a network server for processing. I use [Deluge](http://deluge-torrent.org/) running on my home linux server for this purpose, but TorrentApp will work with alternative BitTorrent applications.

Many BitTorrent applications (Deluge included) offer the ability to install the software on one machine but pass downloads to another instance of the program running on a different machine on the network for download. TorrentApp is intended as a simple, must more lightweight alternative to this paradigm.

## Usage &amp; Setup ##
TorrentApp should be set to "Open" the downloaded torrent file in Windows, although all it actually does is move the file from the default download location to a network share or other folder that is being watched by Deluge running on the server.

TorrentApp then displays a dialog box asking the user if they wish to view the progress of the download. Selecting *Yes* takes the user to Deluge's web interface.

## Configuration ##
The only required configuration is to edit the `TorrentApp.ini` file located in the same folder as the executable. The default content is as follows, and will need to be modified depending on your setup:

````
DestinationFolder=\\Mars\Downloads\TorrentFiles
WebUIAddress=http://internal.jnf.me/deluge
````

The first item, *DestinationFolder*, represents the location of Deluge's "watched" folder.

The second item, *WebUIAddress*, represents the URL of Deluge's web interface. 