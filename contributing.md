Contributing to TorrentApp
==========================

I published TorrentApp to my online code repository mostly as part of the process of learning git, but nevertheless if TorrentApp is interesting to you and you see a way to improve it, you are more than welcome to contribute code.


Authoritative Repository
------------------------

TorrentApp is hosted on gitlab.com. The authoritative repository for TorrentApp can be found here:
https://gitlab.com/jaywll/TorrentApp

If you wish to contribute code to TorrentApp, you will need an account on gitlab.com. It's a free service, open to all.

There's a mirror hosted on code.jnf.me that can be found here:
https://code.jnf.me/jaywll/TorrentApp

By all means feel free to clone or download the project from the mirror, but if you plan to contribute then gitlab.com is the only channel through which to do so.


Contribution Process
--------------------

Once you have a gitlab.com account, here are the steps to making a contribution to TorrentApp's codebase.

### Step One: Fork and Clone Locally ###
From the [project page on gitlab](https://gitlab.com/jaywll/TorrentApp), click the "fork" button. This creates a copy of the repository within your own gitlab account. Next, clone your repository locally. 

### Step Two: Branch ###
Create a feature branch, check it out, and hack away!

### Step Three: Commit and Push ###
Commit the changes you've made, and push the feature branch back to origin (your copy of the repository on gitlab.com).

### Step Four: Merge Request ###
Back on your gitlab.com account, within your forked repository, select merge requests and submit a request to merge your feature branch (`username/TorrentApp/branchname`) with the project's master branch (`jaywll/TorrentApp/master`).

Don't delete your repository yet! Keep an eye on the comments associated with your merge request. There may be further steps requested of you, such as rebasing your feature branch on `jaywll/TorrentApp/master`.


GitLab Flow
-----------

TorrentApp uses a cut-down version of [gitlab flow](https://about.gitlab.com/2014/09/29/gitlab-flow/). As per the instructions above, development takes place on feature branches, and these are merged into the `master` branch when that body work represented by the feature branch is complete.

Periodically, the `master` branch is merged into the `release` branch, and these merge commits are tagged with an appropriate version number. These releases occur at the discretion of the project owner: you should not request to merge a feature branch into `release`, and requests to do so will be rejected.


Binary Files
------------

The project's repository contains compiled binaries. That's probably bad form, but so be it. When you're developing on a feature branch, you are expected to validate that your code compiles successfully (and works as intended!) before submitting a merge request, but please do not include the compiled binaries as part of the commit itself or the subsequent merge request.

The project will be recompiled and the binary files committed by the project owner immediately preceding any numbered release (i.e. immediately before the master branch is merged into the release branch, whenever that occurs).